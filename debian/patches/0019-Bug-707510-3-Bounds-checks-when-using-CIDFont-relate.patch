From: Chris Liddell <chris.liddell@artifex.com>
Date: Wed, 24 Jan 2024 18:25:12 +0000
Subject: Bug 707510(3): Bounds checks when using CIDFont related params
Origin: https://git.ghostscript.com/?p=ghostpdl.git;a=commit;h=7745dbe24514710b0cfba925e608e607dee9eb0f
Bug: https://bugs.ghostscript.com/show_bug.cgi?id=707510
Bug-Debian-Security: https://security-tracker.debian.org/tracker/CVE-2024-29507

Specifically, for CIDFont substitution.
---
 pdf/pdf_font.c     | 45 +++++++++++++++++++++++++++++++++++++++------
 pdf/pdf_warnings.h |  1 +
 2 files changed, 40 insertions(+), 6 deletions(-)

--- a/pdf/pdf_font.c
+++ b/pdf/pdf_font.c
@@ -237,22 +237,55 @@ pdfi_open_CIDFont_substitute_file(pdf_co
                     memcpy(fontfname, fsprefix, fsprefixlen);
                 }
                 else {
-                    memcpy(fontfname, ctx->args.cidfsubstpath.data, ctx->args.cidfsubstpath.size);
-                    fsprefixlen = ctx->args.cidfsubstpath.size;
+                    if (ctx->args.cidfsubstpath.size + 1 > gp_file_name_sizeof) {
+                        code = gs_note_error(gs_error_rangecheck);
+                        pdfi_set_warning(ctx, code, NULL, W_PDF_BAD_CONFIG, "pdfi_open_CIDFont_substitute_file", "CIDFSubstPath parameter too long");
+                        if (ctx->args.pdfstoponwarning != 0) {
+                            goto exit;
+                        }
+                        code = 0;
+                        memcpy(fontfname, fsprefix, fsprefixlen);
+                    }
+                    else {
+                        memcpy(fontfname, ctx->args.cidfsubstpath.data, ctx->args.cidfsubstpath.size);
+                        fsprefixlen = ctx->args.cidfsubstpath.size;
+                    }
                 }
 
                 if (ctx->args.cidfsubstfont.data == NULL) {
                     int len = 0;
-                    if (gp_getenv("CIDFSUBSTFONT", (char *)0, &len) < 0 && len + fsprefixlen + 1 < gp_file_name_sizeof) {
-                        (void)gp_getenv("CIDFSUBSTFONT", (char *)(fontfname + fsprefixlen), &defcidfallacklen);
+                    if (gp_getenv("CIDFSUBSTFONT", (char *)0, &len) < 0) {
+                        if (len + fsprefixlen + 1 > gp_file_name_sizeof) {
+                            code = gs_note_error(gs_error_rangecheck);
+                            pdfi_set_warning(ctx, code, NULL, W_PDF_BAD_CONFIG, "pdfi_open_CIDFont_substitute_file", "CIDFSUBSTFONT environment variable too long");
+                            if (ctx->args.pdfstoponwarning != 0) {
+                                goto exit;
+                            }
+                            code = 0;
+                            memcpy(fontfname + fsprefixlen, defcidfallack, defcidfallacklen);
+                        }
+                        else {
+                            (void)gp_getenv("CIDFSUBSTFONT", (char *)(fontfname + fsprefixlen), &defcidfallacklen);
+                        }
                     }
                     else {
                         memcpy(fontfname + fsprefixlen, defcidfallack, defcidfallacklen);
                     }
                 }
                 else {
-                    memcpy(fontfname, ctx->args.cidfsubstfont.data, ctx->args.cidfsubstfont.size);
-                    defcidfallacklen = ctx->args.cidfsubstfont.size;
+                    if (ctx->args.cidfsubstfont.size > gp_file_name_sizeof - 1) {
+                        code = gs_note_error(gs_error_rangecheck);
+                        pdfi_set_warning(ctx, code, NULL, W_PDF_BAD_CONFIG, "pdfi_open_CIDFont_substitute_file", "CIDFSubstFont parameter too long");
+                        if (ctx->args.pdfstoponwarning != 0) {
+                            goto exit;
+                        }
+                        code = 0;
+                        memcpy(fontfname + fsprefixlen, defcidfallack, defcidfallacklen);
+                    }
+                    else {
+                        memcpy(fontfname, ctx->args.cidfsubstfont.data, ctx->args.cidfsubstfont.size);
+                        defcidfallacklen = ctx->args.cidfsubstfont.size;
+                    }
                 }
                 fontfname[fsprefixlen + defcidfallacklen] = '\0';
 
--- a/pdf/pdf_warnings.h
+++ b/pdf/pdf_warnings.h
@@ -68,4 +68,5 @@ PARAM(W_PDF_INT_AS_REAL,            "fou
 PARAM(PDF_W_NO_TREE_LIMITS,         "Name tree node missing required Limits entry"),
 PARAM(PDF_W_BAD_TREE_LIMITS,        "Name tree node Limits array does not have 2 entries"),
 PARAM(PDF_W_NAMES_ARRAY_SIZE,       "Name tree Names array size not a mulitple of 2"),
+PARAM(W_PDF_BAD_CONFIG,             "A configuration or command line parameter was invalid or incorrect."),
 #undef PARAM
